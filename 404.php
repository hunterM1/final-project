<?php
require_once("includes/config.inc.php");
include_once("includes/loginmodal.inc.php");
include_once("includes/contactmodal.inc.php");

$pageTitle = "Page Not Found";
$pageDescription = "We're sorry, we cannot find the page you are looking for.";
require_once("includes/header.inc.php");
?>
  <div class="container">
    <div class="alert alert-danger text-center mt-5">
      <p><strong>Page not found.</strong> Sorry, we cannot find the page you are looking for.</p>
      <a href="<?php echo(PROJECT_DIR); ?>index.php" class="btn btn-outline-primary">Back to Home</a>
    </div>
  </div>


<?php
include_once("includes/footer.inc.php");
?>