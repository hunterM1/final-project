<?php
require_once("includes/config.inc.php");
include_once("includes/loginmodal.inc.php");
include_once("includes/contactmodal.inc.php");

$pageTitle = "Unauthorized Access";
$pageDescription = "Need to log in to access admin functions.";
require_once("includes/header.inc.php");
?>
  <div class="container">
    <div class="alert alert-warning text-center mt-5">
      <p><strong>Unauthorized Access.</strong> Please login to access admin control panel.</p>
      <a href="<?php echo(PROJECT_DIR); ?>index.php" class="btn btn-outline-primary">Back to Home</a>
    </div>
  </div>


<?php
include_once("includes/footer.inc.php");
?>