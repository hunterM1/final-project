<?php
require_once("includes/config.inc.php");
if($_SERVER['REQUEST_METHOD'] == "POST"){
  $firstName = isset($_POST['contactFirstName']) ? $_POST['contactFirstName'] : NULL;
	$lastName = isset($_POST['contactLastName']) ? $_POST['contactLastName'] : NULL;	
	$email = isset($_POST['contactEmail']) ? $_POST['contactEmail'] : NULL;	
  $comments = isset($_POST['contactMessage']) ? $_POST['contactMessage'] : NULL;
  
if(validateContactData($firstName, $lastName, $email, $comments)){

  // If the data is valid, then put it into a single string to send as an email 
  $msg = "Name: $firstName $lastName <br>";
  $msg .= "Email: $email <br>";
  $msg .= "Comments: $comments";  
  $msg = wordwrap($msg, 70);
  
  sendEmail(ADMIN_EMAIL, "Contact Form Submission", $msg, "From: " . $email);
  $_POST = [];
  header("Location: " . PROJECT_DIR . "contact-confirmation.php");
  exit();	
		
	}else{

    // Foul play suspected (the client-side validation has been bypassed)!
    $msg = getAllSuperGlobals(); 
    sendEmail(ADMIN_EMAIL, "Security Warning!", $msg);
    $_POST = [];
    header("Location: " . PROJECT_DIR . "error.php");
    exit();	
  }
}
else{
  header("Location: " . PROJECT_DIR . "index.php");
  exit();
}

//functions
function validateContactData($firstName, $lastName, $email, $comments){
  $firstName = spam_scrubber($firstName);
  $lastName = spam_scrubber($lastName);
  $email = spam_scrubber($email);
  $comments = spam_scrubber($comments);
  if(empty($firstName) || empty($lastName) || empty($email) || empty($comments)){
    return false;
  }

  if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
    return false;
  }

  return true;
}

function spam_scrubber($value){
  $spam_strings = ['to:', 'cc:', 'bcc:', 'content-type:', 'mime-version:', 'multipart-mixed:', 'content-transfer-encoding:'];

  foreach($spam_strings as $spam){
    if(stripos($value, $spam) !== false){
      return '';
    }
  }

  $value = str_replace(['\r', '\n', '%0a', '%0d'], ' ', $value);

  return trim($value);
}
