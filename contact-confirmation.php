<?php
require_once("includes/config.inc.php");
include_once("includes/loginmodal.inc.php");
include_once("includes/contactmodal.inc.php");

$pageTitle = "Contact Confirmation";
$pageDescription = "Thank you for contacting!";
require_once("includes/header.inc.php");
?>
  <div class="container">
    <div class="alert alert-success text-center mt-5">
      <p><strong>Thank you for contacting me!</strong> I will get back to you as soon as possible.</p>
      <a href="<?php echo(PROJECT_DIR); ?>index.php" class="btn btn-outline-success">Back to Home</a>
    </div>
  </div>
<?php
include_once("includes/footer.inc.php");
?>