<?php
require_once("includes/config.inc.php");
include_once("includes/loginmodal.inc.php");
include_once("includes/contactmodal.inc.php");

$pageTitle = "Error";
$pageDescription = "Script error occurred.";
require_once("includes/header.inc.php");
?>
  <div class="container">
    <div class="alert alert-danger text-center mt-5">
      <p><strong>Oh no!</strong> This website has encountered an error. I humbly apologize, and want you to know that I have been informed of the error and will work diligently to correct it as soon as possible.</p>
      <a href="<?php echo(PROJECT_DIR); ?>index.php" class="btn btn-outline-primary">Back to Home</a>
    </div>
  </div>


<?php
include_once("includes/footer.inc.php");
?>