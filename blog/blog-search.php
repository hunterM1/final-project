<?php
require_once("../includes/config.inc.php");
require_once("../includes/PageDataAccess.inc.php");
include_once("../includes/loginmodal.inc.php");
include_once("../includes/contactmodal.inc.php");
require_once("../includes/displayBlogPosts.inc.php");

$blogHomePage = PROJECT_DIR . "blog/index.php";
$siteLocation = PROJECT_DIR . "blog/blog-search.php";
$searchTerms = "";
if($_SERVER['REQUEST_METHOD'] == "POST"){
  $searchTerms = $_POST['txtBlogSearch'];
}
else{
  $searchTerms = $_GET['searchTerms'];
}
$numToDisplay = 5;

$pda = new PageDataAccess(getDBLink());

$pagesNeeded = $pda->getNumOfPagesNeededBlogSearch($numToDisplay, $searchTerms);
$startIndex = getStartIndexOfDisplayedBlogs();
$blogSearchResults = $pda->getBlogsPerPageBlogSearch($numToDisplay, $startIndex, $searchTerms);

$pageTitle = "Blog Search Results";
$pageDescription = "Search Result for Blogs";
require_once("../includes/header.inc.php");
?>
<div class="container mt-3">
  <div class="card">
    <h2 class="card-header text-center">Blog Post Search Results</h2>
    <div class="card-body">
      <?php
        //display alert to let user know if search results are empty
        if(empty($blogSearchResults)){
          echo("<div class='alert alert-info text-center'>
                  <p>
                    <strong>No blog posts found.</strong> Please try your search again.
                  </p>
                  <a href='$blogHomePage' class='btn btn-outline-primary'>Back to Blog Index Page</a>
                </div>");
        } 
        else{
        //create blog list if search results not empty
          echo(createBlogList($blogSearchResults)); 
          echo("<div class='float-right'><a href='$blogHomePage' class='btn btn-outline-primary'>Back to Blog Index Page</a></div>");
        }
      ?>
    </div>
    <div class="card-footer">
      <?php echo(createBlogPaginationforSearchPage($pagesNeeded, $numToDisplay, $startIndex, $siteLocation, $searchTerms)); ?>
    </div>
  </div>
</div>
<?php
include_once("../includes/footer.inc.php");
?>