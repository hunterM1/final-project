<?php
require_once("../includes/config.inc.php");
include_once("../includes/loginmodal.inc.php");
include_once("../includes/contactmodal.inc.php");
require_once("../includes/PageDataAccess.inc.php");

$pda = new PageDataAccess(getDBLink());
$page = null;

//use pageId query string param to get data for blog page
if(isset($_GET['pageId'])){
  try{
    $page = $pda->getPageById($_GET['pageId']);
  }
  catch(Exception $e){
    redirectTo404Page();
  }
}

$pageTitle = $page['title'];
$pageDescription = $page['description'];
require_once("../includes/header.inc.php");
?>
<div class="container mt-3 mb-5">
  <h1><?php echo($page['title']); ?></h1>
  <p>Date Published: <?php echo($page['publishedDate']); ?></p>
  <p>Blog Category: <?php echo($page['categoryName']); ?></p>
  <p><?php echo($page['description']); ?></p>
  <hr>
  <article>
    <?php echo($page['content']); ?>
  </article>
  <br>
  <a class="btn btn-outline-primary float-right" href="<?php echo(PROJECT_DIR); ?>blog/index.php">Back to Blog Index</a>
</div>
<?php
include_once("../includes/footer.inc.php");
?>