<?php
require_once("../includes/config.inc.php");
require_once("../includes/PageDataAccess.inc.php");
include_once("../includes/loginmodal.inc.php");
include_once("../includes/contactmodal.inc.php");
require_once("../includes/displayBlogPosts.inc.php");

//Set location to blog/index.php for createBlogPagination method
$siteLocation = PROJECT_DIR . "blog/index.php";
//Set number of blog posts to display per page
$numToDisplay = 5;

$pda = new PageDataAccess(getDBLink());

//see how many pages needed for pagination based on num of active blog posts
$pagesNeeded = $pda->getNumOfPagesNeeded(true, $numToDisplay);

//get start index of current page to be used for pagination
$startIndex = getStartIndexOfDisplayedBlogs();
$activePages = $pda->getBlogsPerPage(true, $numToDisplay, $startIndex);


$pageTitle = "Hunter Miller's Blog";
$pageDescription = "Blog about my studies pursuing a web development career.";
require_once("../includes/header.inc.php");
?>
<script src="<?php echo(PROJECT_DIR); ?>js/blog-search-validation.js"></script>
  <div class="container mt-3">
    <div class="card">
      <h2 class="card-header text-center">Blog Posts</h2>
      <div class="card-body">
        <nav class="navbar bg-light mb-2 justify-content-center">
          <form method="POST" id="blogSearchForm" action="<?php echo(PROJECT_DIR); ?>blog/blog-search.php" class="form-inline justify-content-center">
            <label for="txtBlogSearch" class="mt-3">Search for blog post:</label>
            <input class="form-control mr-sm-2 mt-3 ml-2" type="text" name="txtBlogSearch" id="txtBlogSearch">
            <input class="btn btn-outline-info mt-3" type="submit" value="Search"></input>
            <span class="text-danger mt-3 ml-2" id="vBlogSearch"></span>
          </form>
        </nav>
        <?php echo(createBlogList($activePages)); ?>
      </div>
      <div class="card-footer">
        <?php echo(createBlogPagination($pagesNeeded, $numToDisplay, $startIndex, $siteLocation)); //create blog pagination if needed ?>
      </div>
    </div>
  </div>
<?php
include_once("../includes/footer.inc.php");
?>