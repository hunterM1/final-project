<?php
require_once("../includes/config.inc.php");
require("authentication-check.inc.php");
require_once("../includes/CategoryDataAccess.inc.php");
include_once("../includes/loginmodal.inc.php");
include_once("../includes/contactmodal.inc.php");

$pageTitle = "Category Details";
$pageDescription = "";

//Set defaults
$category = array();
$category['categoryId'] = 0;
$category['name'] = "";
$category['active'] = "no";

$cda = new CategoryDataAccess(getDBLink());

// Create an empty array to store input validation errors
$validationErrors = array();


if($_SERVER['REQUEST_METHOD'] == "GET"){
	
	if(isset($_GET['categoryId'])){
    $category = $cda->getCategoryById($_GET['categoryId']);
	}

}elseif($_SERVER['REQUEST_METHOD'] == "POST"){

  // Get the user input and stuff it into the $category array
  $category['categoryId'] = $_POST['categoryId'];
  $category['name'] = $_POST['name']; 
  $category['active'] = $_POST['active'] ?? $category['active']; 

  // Validate the input and get validation errors (if there are any)
  $validationErrors = validateCategoryInput($category);

  if(empty($validationErrors)){
    // the input is valid, so we'll send it to the database
    // But we need to determine whether to INSERT or UPDATE

	// Insert or Update (depends on $category['categoryId'])
	if($category['categoryId'] > 0){
		// UPDATE
		$cda->updateCategory($category);
	}else{
		// INSERT
		$cda->insertCategory($category);
	}

	header("Location: " . PROJECT_DIR . "control-panel/category-list.php");
	exit();
  }
}else{
	// we only accept GET and POST requests
	header("Location: " . PROJECT_DIR . "error.php");
	exit();
}

require_once("../includes/header.inc.php");
?>
<div class="container">
  <div class="row bg-light justify-content-center mt-4">
    <h2 class="mt-1">Category Details</h2>
  </div>
  <div class="card w-100 mt-4">
    <div class='card-body'>
      <form method="POST" action="<?php echo($_SERVER['PHP_SELF']) ?>">
    
        <input type="hidden" name="categoryId" value="<?php echo($category['categoryId']); ?>" />
        
        <div class='form-group'>
          <label>
            Name
            <?php echo(isset($validationErrors['name']) ? wrapValidationMsg($validationErrors['name']) : ""); ?>
          </label>
          <input class='form-control' type="text" name="name" value="<?php echo($category['name']); ?>" />
        </div>
        
        <div class="form-group">
          <label>
            Active
            <?php echo(isset($validationErrors['active']) ? wrapValidationMsg($validationErrors['active']) : ""); ?>
          </label>
          <div class="custom-control custom-radio">
            <input id="categoryRadio1" class="custom-control-input" type="radio" name="active" value="yes" <?php echo($category['active'] == "yes" ? "checked" : "") ?> />
            <label class="custom-control-label" for="categoryRadio1">YES</label>
          </div>
          <div class="custom-control custom-radio">
            <input id="categoryRadio2" class="custom-control-input" type="radio" name="active" value="no" <?php echo($category['active'] == "no" ? "checked" : "") ?> />
            <label class="custom-control-label" for="categoryRadio2">NO</label>
          </div>
        </div>

        <input type="submit" value="Save" class="btn btn-outline-primary float-right btn-lg"/>	
      </form>
    </div>
	</div>
</div>
		
<?php
include_once("../includes/footer.inc.php");

function validateCategoryInput($category){

	// we'll populate this array with any errors that we discover.
	$errors = array();

	// validate name
	if(empty($category['name'])){
		$errors['name'] = "You must enter a category name";
	}

	// validate active
	if($category['active'] != "yes" && $category['active'] != "no"){
		// foul play suspected!
		$errors['active'] = "Active must be 'yes' or 'no'.";
	}

	return $errors;
}
?>