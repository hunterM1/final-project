<?php
require_once("../includes/config.inc.php");
require("authentication-check.inc.php");
require_once("../includes/FileDataAccess.inc.php");
require_once("../includes/ImageUploader.inc.php");
include_once("../includes/loginmodal.inc.php");
include_once("../includes/contactmodal.inc.php");

$pageTitle = "File Details";
$pageDescription = "";

//Set defaults
$file = array();
$file['fileId'] = 0;
$file['fileName'] = "";
$file['fileDescription'] = "";
$file['fileExtension'] = "";
$file['fileSize'] = 0;

// Set up the $fda object 
$fda = new FileDataAccess(getDBLink());

// Create an empty array to store input validation errors
// (we'll use this array when we get to the validation code)
$validationErrors = array();
// we'll allow these types of files to be uploaded
$allowed_file_types = array('image/pjpeg','image/jpeg','image/JPG','image/X-PNG','image/PNG','image/png','image/x-png');
// the images must be less than 1MB (1000000 bytes)
$max_file_size = 1000000;

if($_SERVER['REQUEST_METHOD'] == "GET"){
	
	if(isset($_GET['fileId'])){
    $file = $fda->getFileById($_GET['fileId']); 
	}

}elseif($_SERVER['REQUEST_METHOD'] == "POST"){
  
  //Set $filePath so that error does not occur if admin edits description of file without changing it
  $filePath = "";

	// Get the user input and stuff it into the $page array
	$file['fileId'] = $_POST['fileId']; // hidden input
	$file['fileName'] = $_POST['fileName']; // hidden input
	$file['fileSize'] = $_POST['fileSize']; // hidden input
	$file['fileExtension'] = $_POST['fileExtension']; // hidden input
	$file['fileDescription'] = $_POST['fileDescription']; 
	
	// Validate the input and get validation errors (if there are any)
	$validationErrors = validateFileInput($file, $allowed_file_types, $max_file_size);

	if(empty($validationErrors)){

		// process the file upload
		if($_FILES['upload']['error'] == UPLOAD_ERR_OK){
	
      // get details of the uploaded file and store them in the $file array
      $file['fileName'] = $_FILES['upload']['name'];
      $file['fileSize'] = $_FILES['upload']['size']; 
      $file['fileExtension'] = $fda->getFileExtension($file['fileName']);
    
      // set the full path of where the uploaded file should be moved to
      $filePath = SERVER_UPLOAD_FOLDER . $file['fileName'];
    
        // move the file from the tmp dir to it's final destination
        if( !move_uploaded_file($_FILES['upload']['tmp_name'], $filePath) ){
          throw new Exception("Unable to move uploaded file");
        }
    }

		// Insert or Update (depends on $file['fileId'])
		if($file['fileId'] > 0){
			// UPDATE
      $file = $fda->updateFile($file);
		}else{
			// INSERT
			$file = $fda->insertFile($file);
		}

	  // Now that the image has been uploaded, let's rename it so that it
    // is named by the file id, rather than the original name
    $newFileName = $file['fileId'] . "." . $file['fileExtension'];
    $newFilePath = SERVER_UPLOAD_FOLDER . $newFileName;

    if($filePath != ""){
      if(!rename($filePath, $newFilePath) ){
        throw new Exception("Unable to rename file");
      }  		
    }

    // Create a thumbnail of the image 
    $imgUploader = new ImageUploader($max_file_size, $allowed_file_types);
    $thumbNailPath = SERVER_THUMBNAIL_FOLDER . $newFileName;
    $resize_width = 150;								
    $resize_height = 200;

    // resize the image...
    $resize_result = $imgUploader->resizeImage($newFilePath, $thumbNailPath, $resize_width, $resize_height);

    // $resize_result will be an array that has some details about the final size of the image
    if($resize_result ==  false){
      throw new Exception("Unable to resize the image.");
    }
			
		header("Location: " . PROJECT_DIR . "control-panel/file-list.php");
		exit();
	}
	

}else{
	// we only accept GET and POST requests
	header("Location: " . PROJECT_DIR . "error.php");
	exit();
}

require_once("../includes/header.inc.php");
?>
<script src="<?php echo(PROJECT_DIR); ?>js/display-filename.js"></script>
<div class="container">
  <div class="row bg-light justify-content-center mt-4">
    <h2 class="mt-1">File Details</h2>
  </div>
  
  <div class="card w-100 mt-4">
    <!--DON'T FORGET TO SET THE enctype ATTRIBUTE ON THE FORM TAG-->
    <form method="POST" enctype="multipart/form-data" action="<?php echo($_SERVER['PHP_SELF']) ?>">
      
      <input type="hidden" name="fileId" value="<?php echo($file['fileId']); ?>" />
      <input type="hidden" name="fileName" value="<?php echo($file['fileName']); ?>" />
      <input type="hidden" name="fileExtension" value="<?php echo($file['fileExtension']); ?>" />
      <input type="hidden" name="fileSize" value="<?php echo($file['fileSize']); ?>" />
      
      <?php
      if($file['fileId'] > 0 && !empty($file['fileExtension'])){
        $realFileName = $file['fileId'] . "." . $file['fileExtension'];
        echo('<img class="card-img-top" src="' . UPLOAD_FOLDER . $realFileName .'" />');
      }
      ?>
      <div class='card-body'>
        <p class="text-center">
          <?php
          // display error messages related to the file upload (if there are any to display)
          echo(isset($validationErrors['imageRequired']) ? wrapValidationMsg($validationErrors['imageRequired']) . "<br>" : "");
          echo(isset($validationErrors['invalidImageType']) ? wrapValidationMsg($validationErrors['invalidImageType']) . "<br>" : "");
          echo(isset($validationErrors['invalidImageSize']) ? wrapValidationMsg($validationErrors['invalidImageSize']) . "<br>" : "");
          ?>
        </p>
        <div class='form-group'>
          <div class="custom-file" id="customFile">
            <input type="file" name="upload" class="custom-file-input" id="fileInput"/>
            <label class="custom-file-label" for="fileInput">Select file</label>
          </div>
        </div>
        <div class="form-group mt-1 text-center">
          <label for="fileDescription">
            Description
            <?php echo(isset($validationErrors['fileDescription']) ? wrapValidationMsg($validationErrors['fileDescription']) : ""); ?>
          </label>
          <textarea class="form-control" name="fileDescription" id="fileDescription"><?php echo($file['fileDescription']); ?></textarea>
        </div>
        <input type="submit" value="Save" class="btn btn-outline-primary float-right btn-lg"/>
      </div>
    </form>
  </div>
</div>
		
<?php
include_once("../includes/footer.inc.php");

function validateFileInput($file, $allowed_file_types, $max_file_size){

	// we'll populate this array with any errors that we discover.
	$errors = array();

	if(empty($file['fileDescription'])){
		$errors['fileDescription'] = "You must enter a description";
	}

	// on inserts, there must be a file uploaded
  // http://php.net/manual/en/features.file-upload.errors.php
  if($file['fileId'] == 0 && $_FILES['upload']['error'] == UPLOAD_ERR_NO_FILE){
    $errors['imageRequired'] = "You must upload an image.";
  }

  if($_FILES['upload']['error'] == UPLOAD_ERR_OK){
    
    // validate the file type
    if( !in_array($_FILES['upload']['type'], $allowed_file_types) ){
      $errors['invalidImageType'] = "The file must be an image.";
    }
    
    // validate the file size
    if($_FILES['upload']['size'] > $max_file_size){
      $errors['invalidImageSize'] = "The file is too big.";
    }
  }

	return $errors;
}

?>