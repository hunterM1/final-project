<?php
require_once("../includes/config.inc.php");
require("authentication-check.inc.php");
require_once("../includes/CategoryDataAccess.inc.php");
include_once("../includes/loginmodal.inc.php");
include_once("../includes/contactmodal.inc.php");

$cda = new CategoryDataAccess(getDBLink());
$categories = $cda->getCategoryList(false);

$pageTitle = "Category List";
$pageDescription = "";

require_once("../includes/header.inc.php");
?>
<div class="container mb-5">
  <div class="row bg-light justify-content-center mt-4">
    <h2 class="mt-1">Category List</h2>
  </div>
  <?php
    echo(displayCategories($categories));
  ?>
  <div class="row justify-content-center">
    <a href="category-details.php" class="btn btn-outline-primary float-right">Add New Category</a>
  </div>
</div>

<?php
include_once("../includes/footer.inc.php");

function displayCategories($categories){

  $html = "<table class='table table-sm table-striped'>";

  $html .= "<thead>
              <tr>
                <th>Name</th>
                <th>Active</th>
                <th>Edit</th>
              </tr>
            </thead>
            <tbody>";

  foreach($categories as $category){
    $html .= "<tr>";
    $html .= "<td>{$category['name']}</td>";
    $html .= "<td>{$category['active']}</td>";
    $html .= "<td><a href=\"category-details.php?categoryId={$category['categoryId']}\">EDIT</a></td>";
    $html .= "</tr>";
  }

  $html .= "</tbody>
            </table>";


  return $html;
}
?>