<?php
if(empty($_SESSION['authenticated']) || $_SESSION['authenticated'] !== "yes"){
	header("Location: " . PROJECT_DIR . "unauthorized-access.php");
	exit();
}