<?php
require_once("../includes/config.inc.php");
require("authentication-check.inc.php");
require_once("../includes/PageDataAccess.inc.php");
include_once("../includes/loginmodal.inc.php");
include_once("../includes/contactmodal.inc.php");
require_once("../includes/displayBlogPosts.inc.php");

$siteLocation = PROJECT_DIR . "control-panel/blog-list.php";
$numToDisplay = 20;

$pda = new PageDataAccess(getDBLink());

//get num pages needed with any pages- active or not
$pagesNeeded = $pda->getNumOfPagesNeeded(false, $numToDisplay);

//get start index of current page to be used for pagination
$startIndex = getStartIndexOfDisplayedBlogs();
$allPages = $pda->getBlogsPerPage(false, $numToDisplay, $startIndex);

$pageTitle = "Blog List";
$pageDescription = "";

require_once("../includes/header.inc.php");
?>
<div class="container mb-5">
  <div class="row bg-light justify-content-center mt-4">
    <h2 class="mt-1">Blog List</h2>
  </div>
  <?php
    echo(displayPages($allPages));
    if($pagesNeeded > 1){
      echo("<div class='row justify-content-end'>");
      echo(createBlogPagination($pagesNeeded, $numToDisplay, $startIndex, $siteLocation));
      echo("</div>");
    }
  ?>
  <div class="row justify-content-center">
    <a href="blog-details.php" class="btn btn-outline-primary float-right">Add New Blog Page</a>
  </div>
</div>

<?php
include_once("../includes/footer.inc.php");

function displayPages($pages){
  $html = "<table class='table table-sm table-striped'>";

  $html .= "<thead>
              <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Publish Date</th>
                <th>Active</th>
                <th>Edit</th>
              </tr>
            </thead>
            <tbody>";

  foreach($pages as $page){
    $html .= "<tr>";
    $html .= "<td>{$page['title']}</td>";
    $html .= "<td>{$page['description']}</td>";
    $html .= "<td>{$page['publishedDate']}</td>";
    $html .= "<td>{$page['active']}</td>";
    $html .= "<td><a href=\"blog-details.php?pageId={$page['pageId']}\">EDIT</a></td>";
    $html .= "</tr>";
  }

  $html .= "</tbody>
            </table>";

  return $html;
}
?>