<?php
require_once("../includes/config.inc.php");
require("authentication-check.inc.php");
require_once("../includes/FileDataAccess.inc.php");
include_once("../includes/loginmodal.inc.php");
include_once("../includes/contactmodal.inc.php");

$fda = new FileDataAccess(getDBLink());
$files = $fda->getFileList(false);

$pageTitle = "File List";
$pageDescription = "";

require_once("../includes/header.inc.php");
?>
<div class="container mb-5">
  <div class="row bg-light justify-content-center mt-4">
    <h2 class="mt-1">File List</h2>
  </div>
  <?php
    echo(displayFiles($files));
  ?>
  <div class="row justify-content-center">
    <a href="file-details.php" class="btn btn-outline-primary float-right">Add New File</a>
  </div>
</div>
		
<?php
include_once("../includes/footer.inc.php");

function displayFiles($files){

	$html = "<table class='table table-sm table-striped'>";

  $html .= "<thead>
              <tr>
              <th>File Name</th>
              <th>File Description</th>
              <th>File Extension</th>
              <th>File Size</th>
              <th>Edit File</th>
              </tr>
            </thead>
            <tbody>";
	
	// create table rows (loop through the files)
	foreach($files as $file){
    $html .= "<tr>";
    $html .= "<td>{$file['fileName']}</td>";
    $html .= "<td>{$file['fileDescription']}</td>";
    $html .= "<td>{$file['fileExtension']}</td>";
    $html .= "<td>{$file['fileSize']}</td>";
    $html .= "<td><a href=\"file-details.php?fileId={$file['fileId']}\">EDIT</a></td>";
    $html .= "</tr>";
  }

  $html .= "</tbody>
            </table>";

  return $html;
}

?>