<?php
require_once("../includes/config.inc.php");
require_once("authentication-check.inc.php");
include_once("../includes/loginmodal.inc.php");
include_once("../includes/contactmodal.inc.php");

$pageTitle = "Control Panel";
$pageDescription = "";
include_once("../includes/header.inc.php");
?>
<div class="container">
  <div class='alert alert-info mt-5'>
    <h2 class="alert-heading text-center">Admin Control Panel</h2>
    <br>
    <div class='col-lg-4 offset-lg-4'>
      <a class='btn btn-block btn-dark btn-lg' href="blog-list.php">Blog List</a>
      <br>
      <br>
      <a class='btn btn-block btn-dark btn-lg' href="file-list.php">File List</a>
      <br>
      <br>
      <a class='btn btn-block btn-dark btn-lg' href="category-list.php">Category List</a>
      <br>
      <br>
    </div>
  </div>
</div>
<?php
include_once("../includes/footer.inc.php");
?>