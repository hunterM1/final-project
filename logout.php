<?php
require_once("includes/config.inc.php");
//User goes to this page when logging out- sends them back to home page
if(isset($_COOKIE[session_name()])){
      setcookie(session_name(), "", time()-3600, "/");
    }
//empty the $SESSION array
$_SESSION = array();
//destroy the session on the server
session_destroy();
header("Location: " . PROJECT_DIR . "index.php");
exit();