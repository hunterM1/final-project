window.addEventListener("load", function(){

  var blogSearchForm = document.getElementById('blogSearchForm');
  var vBlogSearch = document.getElementById('vBlogSearch');
  var txtBlogSearch = document.getElementById('txtBlogSearch');

  blogSearchForm.addEventListener("submit", function(e){
    if(validateBlogSearchForm() == false){
      e.preventDefault();
    }
  });

  function validateBlogSearchForm(){
    var formIsValid = true;

    if(txtBlogSearch.value == ""){
      vBlogSearch.textContent = "You must enter a search term";
      formIsValid = false;
    }
    return formIsValid;
  }
});