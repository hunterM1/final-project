$(document).on('show.bs.modal', '#contactModal', function(){

  var contactForm = document.getElementById("contactForm");
  
  // form inputs
  var contactFirstName = document.getElementById("contactFirstName");
  var contactLastName = document.getElementById("contactLastName");
  var contactEmail = document.getElementById("contactEmail");
  var contactMessage = document.getElementById("contactMessage");

  // validation spans
  var vFirstName = document.getElementById("vFirstName");
  var vLastName = document.getElementById("vLastName");
  var vEmail = document.getElementById("vEmail");
  var vMessage = document.getElementById("vMessage");
  
  // event handler for when the form is submitted
  contactForm.addEventListener("submit", function(e){
    if(validateContactForm() == false){
      e.preventDefault();
    }
  });

  // this function should return false if the input is not valid
  // it should return true if the input is valid
  function validateContactForm(){
    var formIsValid = true;

    clearValidationMessages();
    
      // validate the first name entered
      if(contactFirstName.value == ""){
        vFirstName.innerHTML = "Please enter your first name";
        vFirstName.style.display = "block";
        formIsValid = false;
      }

      // validate the last name entered
      if(contactLastName.value == ""){
        vLastName.innerHTML = "Please enter your last name";
        vLastName.style.display = "block";
        formIsValid = false;
      }

      // validate the email entered
      if(contactEmail.value == ""){
        vEmail.innerHTML = "Please enter your email";
        vEmail.style.display = "block";
        formIsValid = false;
      }else if(validateEmailAddress(contactEmail.value) == false){
        vEmail.innerHTML = "The email you entered is not valid";
        vEmail.style.display = "block";
        formIsValid = false;
      }

      // validate the comments entered
      if(contactMessage.value == ""){
        vMessage.innerHTML = "Please enter some comments";
        vMessage.style.display = "block";
        formIsValid = false;
      }else if(containsURL(contactMessage.value)){
        vMessage.innerHTML = "URLs are not allowed in the comments";
        vMessage.style.display = "block";
        formIsValid = false;
      }

      return formIsValid;
  }

  // clears out all the validation messages
  function clearValidationMessages(){
      var divs = document.querySelectorAll(".text-danger");
      for(var x = 0;  x < divs.length; x++){
          divs[x].innerHTML = "";
          divs[x].style.display = "none";
      }
  }

  // checks a string to see if a URL is in it (returns true if the string has a URL in it, false if not)
  function containsURL(str){
      var regExp = /\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i;
      return regExp.test(str);
  }

  // validates an email address (returns true it is valid, false if it is not)
  function validateEmailAddress(email){
      var regExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regExp.test(email);
  }


});