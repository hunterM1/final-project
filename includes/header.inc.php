<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?php echo($pageDescription); ?>">
  <!-- Development technology icons -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/devicons/devicon@master/devicon.min.css">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo(PROJECT_DIR); ?>styles/main.css">
  <link rel="stylesheet" type="text/css" href="<?php echo(PROJECT_DIR); ?>styles/navbar.css">
  <link rel="stylesheet" type="text/css" href="<?php echo(PROJECT_DIR); ?>styles/header.css">
  <script src="<?php echo(PROJECT_DIR); ?>js/client-side-login-validation.js"></script>
  <title><?php echo($pageTitle); ?></title>
</head>
<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top w-100">
  <a class="navbar-brand nav-link" id="nav-title" href="<?php echo(PROJECT_DIR); ?>index.php">Hunter Miller - Student Developer</a>
  <button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse" aria-controls="my-nav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="my-nav">
    <ul class="navbar-nav ml-auto mt-2">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo(PROJECT_DIR); ?>index.php">About</a>
      </li>
      <li class="nav-item">
      <a class="nav-link" href="<?php echo(PROJECT_DIR); ?>blog/index.php">Blog</a>
      </li>
      <li class="nav-item">
        <span class="nav-link" data-toggle="modal" data-target="#contactModal">Contact</span>
      </li>
      <?php

    //Add control panel link if user logged in
    //Show login or log out button depending on if user is logged in or not
    if(isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == "yes"){
      $control_panel_link = PROJECT_DIR . "control-panel/index.php";
      echo("<li class='nav-item'><a class='nav-link' href='$control_panel_link'>Control Panel</a></li>");
      //If user logged in, clicking button takes them to logout.php and destroyes cookies/sessions. logout.php then redirects them to home page.
      $logout_php_file_locat = PROJECT_DIR . "logout.php";
      echo("<li class='nav-item'><form action='$logout_php_file_locat'><input type='submit' value='Log Out' id='logoutBtn' class='btn btn-default navbar-btn'/></form></li>");
    }
    else{
      //Button for login modal
      echo('<li class="nav-item"><button id="loginBtn" class="btn btn-default navbar-btn" type="button" data-toggle="modal" data-target="#loginModal">Admin Login</button></li>');
    }

    ?>
    </ul>
  </div>
</nav>
<body>

