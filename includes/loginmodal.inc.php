<?php
require_once("config.inc.php");
?>
<div class="modal fade" id="loginModal" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <form method="POST" id="loginForm" action="<?php echo(PROJECT_DIR); ?>login.php">
          <div class="form-group">
            <label for="txtUserName">User Name:</label>
            <input type="text" name="txtUserName" class="form-control" id="txtUserName">
            <label for="txtPassword">Password:</label>
            <input type="password" name="txtPassword" class="form-control" id="txtPassword">
          </div>
          <div class="modal-footer">
            <input type="submit" value="Log In" class="col-12 text-center btn btn-outline-primary" id="submitLoginBtn"></input>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>