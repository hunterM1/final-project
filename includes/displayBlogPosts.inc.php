<?php
//function to create the blog listings
function createBlogList($pages){
  $blog_directory = PROJECT_DIR . "blog/";
  $html = "";

  foreach($pages as $p){
    $blog_id = $p['pageId'];
    $blog_title = $p['title'];
    $blog_description = $p['description'];
    $blog_date = $p['publishedDate'];
    $blog_category = $p['categoryName'];

    $html .= "<div class='card mb-3'>
              <div class='card-body'>";
    $html .=  "<h3 class='text-center card-title'><a class='text-info' href='" . $blog_directory . "blog-post.php?pageId=$blog_id'>$blog_title</a></h3>";
    $html .= "<div class='card-subtitle text-muted mb-5'>";
    $html .= "<span class='float-left float-sm-left'>Date Published: $blog_date</span>";
    $html .= "<span class='float-left float-sm-right'>Blog Category: $blog_category</span>";
    $html .= "</div>";
    $html .= "<br>";
    $html .= "<div class='card-text'><p>$blog_description</p></div>";
    $html .= "</div>
              </div>";
  }

  return $html;
}

//function to create blog pagination
function createBlogPagination($pagesNeeded, $numToDisplay, $startIndex, $siteLocation){

  if($pagesNeeded > 1){
    $html = "<ul class='pagination float-right mt-3'>";
    
    //Determine page script is on currently
    $current_page = ($startIndex/$numToDisplay) + 1;

    //if not first page, make previous link available
    if($current_page != 1){
      $link_for_previous = $startIndex - $numToDisplay;
      $html .= "<li class='page-item'>
                  <a class='page-link' href='$siteLocation?s=$link_for_previous'>Previous</a>
                </li>";
    }
    else{
      $html .= "<li class='page-item disabled'>
                  <a class='page-link' tabindex='-1' href='#'>Previous</a>
                </li>";
    }

    //make all numbered pages
    for($i = 1; $i <= $pagesNeeded; $i++){
      if ($i != $current_page){
        $link_to_num = $numToDisplay * ($i - 1);
        $html .= "<li class='page-item'>
                    <a class='page-link' href='$siteLocation?s=$link_to_num'>$i</a>
                  </li>";
      }
      else{
        $html .= "<li class='page-item active'>
                    <a class='page-link' href='#'>$i<span class='sr-only'>(current)</span></a>
                  </li>";
      }
    }

    //if not last page, make next button available
    if($current_page != $pagesNeeded){
      $link_for_next = $startIndex + $numToDisplay;
      $html .= "<li class='page-item'>
                  <a class='page-link' href='$siteLocation?s=$link_for_next'>Next</a>
                </li>";
    }
    else{
      $html .= "<li class='page-item disabled'>
                  <a class='page-link' tabindex='-1' href='#'>Next</a>
                </li>";
    }

    $html .= "</ul>";

    return $html;
  }
  else{
    return;
  }
}

//function to create blog pagination for search page
function createBlogPaginationforSearchPage($pagesNeeded, $numToDisplay, $startIndex, $siteLocation, $searchTerms){

  if($pagesNeeded > 1){
    $html = "<ul class='pagination float-right mt-3'>";
    
    //Determine page script is on currently
    $current_page = ($startIndex/$numToDisplay) + 1;

    //if not first page, make previous link available
    if($current_page != 1){
      $link_for_previous = $startIndex - $numToDisplay;
      $html .= "<li class='page-item'>
                  <a class='page-link' href='$siteLocation?s=$link_for_previous&searchTerms=$searchTerms'>Previous</a>
                </li>";
    }
    else{
      $html .= "<li class='page-item disabled'>
                  <a class='page-link' tabindex='-1' href='#'>Previous</a>
                </li>";
    }

    //make all numbered pages
    for($i = 1; $i <= $pagesNeeded; $i++){
      if ($i != $current_page){
        $link_to_num = $numToDisplay * ($i - 1);
        $html .= "<li class='page-item'>
                    <a class='page-link' href='$siteLocation?s=$link_to_num&searchTerms=$searchTerms'>$i</a>
                  </li>";
      }
      else{
        $html .= "<li class='page-item active'>
                    <a class='page-link' href='#'>$i<span class='sr-only'>(current)</span></a>
                  </li>";
      }
    }

    //if not last page, make next button available
    if($current_page != $pagesNeeded){
      $link_for_next = $startIndex + $numToDisplay;
      $html .= "<li class='page-item'>
                  <a class='page-link' href='$siteLocation?s=$link_for_next&searchTerms=$searchTerms'>Next</a>
                </li>";
    }
    else{
      $html .= "<li class='page-item disabled'>
                  <a class='page-link' tabindex='-1' href='#'>Next</a>
                </li>";
    }

    $html .= "</ul>";

    return $html;
  }
}

//function to get start index in database to display correct blog posts
function getStartIndexOfDisplayedBlogs(){
  if(isset($_GET['s']) && is_numeric($_GET['s'])){
    $start = $_GET['s'];
  }
  else{
    $start = 0;
  }
  return $start;
}