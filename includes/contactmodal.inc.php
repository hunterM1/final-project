<?php
require_once("config.inc.php");
?>
<div class="modal fade" id="contactModal" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Contact Me</h5>
      </div>
      <div class="modal-body">
        <form method="POST" id="contactForm" action="<?php echo(PROJECT_DIR); ?>contact-verification.php">
          <div class="form-group">
            <label for="contactFirstName">First Name:</label>
            <span class="text-danger" id="vFirstName"></span>
            <input type="text" class="form-control" id="contactFirstName" name="contactFirstName">
            <br>
            <label for="contactLastName">Last Name:</label>
            <span class="text-danger" id="vLastName"></span>
            <input type="text" class="form-control" id="contactLastName" name="contactLastName">
            <br>
            <label for="contactEmail">Email:</label>
            <span class="text-danger" id="vEmail"></span>
            <input type="text" class="form-control" id="contactEmail" name="contactEmail">
            <br>
            <label for="contactMessage">Message:</label>
            <span class="text-danger" id="vMessage"></span>
            <textarea class="form-control" id="contactMessage" name="contactMessage"></textarea>
          </div>
          <div class="modal-footer">
            <input type="submit" value="Submit" class="col-12 text-center btn btn-outline-primary"></input>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
