<?php
require_once("includes/config.inc.php");
include_once("includes/loginmodal.inc.php");
include_once("includes/contactmodal.inc.php");

$pageTitle = "Hunter Miller - Web Developer";
$pageDescription = "Website about Hunter Miller- Student at Western Technical College for Web and Software Development.";
require_once("includes/header.inc.php");
?>
  <div class="container-fluid">
    <div class="jumbotron">
      <div class="row">
        <div class="col-lg-8 col-sm-12">
          <h1 class="display-3 web-title-main">Hunter Miller</h1>
          <hr class="text-secondary">
          <br>
          <br>
          <p class="title-second">Web and software development student at Western Technical College.</p>
        </div>
          <div class="col-lg-4" id="header-photo-container">
            <img id="header-photo" src="<?php echo(PROJECT_DIR); ?>assets/pictures/me.jpg">
          </div>
        </div>
    </div>
  </div>
  <div class="container-fluid mb-5">
    <div class="card mb-4">
      <h4 class="card-header text-center">About Me</h4>
      <div class="card-body">
        <p class="card-text ml-5 mr-5">Hello! My name is Hunter Miller, and I am from a small town near La Crosse, Wisconsin. Growing up, I've always enjoyed playing sports and have been racing cars at the local track since I was 15 years old. I am currently a student at Western Technical College in La Crosse, Wisconsin pursuing a degree to become a web and software developer in the future. Take a look at some of the programming technologies that I have come across and have experienced, check out my blog, and feel free to contact me if you have any more questions!</p>
      </div>
    </div>
    <div class="card">
      <h4 class="card-header text-center">Programming Technologies</h4>
      <div class="card-body">
        <div class="card-deck">
          <div class="card mb-sm-2">
            <div class="card-body">
              <h1 class="card-title text-center"><i class="devicon-html5-plain-wordmark"></i></h1>
              <p class="card-text">Of course, as a web developer my first interaction with creating web pages involved using html5 markup. I have used this extensively throughout my projects creating websites and expanded on its use with the Angular framework to create dynamic web pages.</p>
            </div>
          </div>
          <div class="card mb-sm-2">
            <div class="card-body">
              <h1 class="card-title text-center"><i class="devicon-css3-plain-wordmark colored"></i></h1>
              <p class="card-text">The use of CSS3 has been used throughout all of my projects to incorporate a style that is responsive and clean. This includes utilizing frameworks such as bootstrap to create web pages that look sleek and perform on a variety of devices.</p>
            </div>
          </div>
        </div>
        <div class="card-deck">
          <div class="card mb-sm-2">
            <div class="card-body">
            <h1 class="card-title text-center"><i class="devicon-javascript-plain colored"></i></h1>
              <p class="card-text">The first use of “real” programming in my studies have been involved with manipulating DOM objects in a webpage with JavaScript. Take a look at some of my earlier projects such as the online rock-paper-scissors game or the in-browser etch-a-sketch that I created to get a glimpse at how I utilized JavaScript earlier in my studies.</p>
            </div>
          </div>
          <div class="card mb-sm-2">
            <div class="card-body">
            <h1 class="card-title text-center"><i class="devicon-java-plain-wordmark"></i></h1>
              <p class="card-text">Java was the first language that we studied in our curriculum that was a real object-orientated programming language. This allowed me to understand basic OOP principles to further my understanding of programming.</p>
            </div>
          </div>
        </div>
        <div class="card-deck">
          <div class="card mb-sm-2">
            <div class="card-body">
            <h1 class="card-title text-center"><i class="devicon-csharp-line colored"></i></h1>
              <p class="card-text">C# is a language that I personally love with its capability to use the .NET Core framework. A few of my personal projects utilized the .NET Core framework as the back-end to websites with the MVC model.</p>
            </div>
          </div>
          <div class="card mb-sm-2">
            <div class="card-body">
            <h1 class="card-title text-center"><i class="devicon-angularjs-plain colored"></i></h1>
              <p class="card-text">Many of my projects have utilized Angular on the front-end to create dynamic web pages. Angular has been a personal favorite of mine on the front-end with its relative ease to deliver input validation.</p>
            </div>
          </div>
        </div>
        <div class="card-deck">
          <div class="card">
            <div class="card-body">
            <h1 class="card-title text-center"><i class="devicon-android-plain-wordmark colored"></i></h1>
              <p class="card-text">Android development was a part of the core curriculum at Western Technical College, giving us a glimpse on how to create applications on the mobile side of development.</p>
            </div>
          </div>
          <div class="card">
            <div class="card-body">
            <h1 class="card-title text-center"><i class="devicon-git-plain colored"></i></h1>
              <p class="card-text">All of my development projects, whether for school or personal, have utilized Git to back-up and log changes made throughout development.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php
include_once("includes/footer.inc.php");
?>